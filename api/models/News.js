/**
* News.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

    attributes: {

        "title" : {
            "type" : "string",
            "required" : true
        },

        "content" : {
            "type" : "string",
            "required" : true
        },

        "index" : {
            "type" : "integer",
            "defaultsTo" : 0
        }
    },

    beforeCreate: function(obj, next){

        News
            .find({})
            .sort({ index : 'desc' })
            .limit(1)
            .exec(function (err, items) {
                if(err) return next(err);
                News
                    .count()
                    .exec(function(err, cnt) {
                        if(err) return next(err);
                        var count = !items.length ? 0 : parseInt(items[0].index);
                        if (isNaN(count)) count = 0;
                        if (count < cnt)  count = cnt;
                        count++;
                        obj['index'] = count;
                        next(null);
                    });
            });
    }
};

