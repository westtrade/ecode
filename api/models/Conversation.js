"use strict";

/**
* Conversation.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/



var uuid = require('node-uuid');
var Promise = require('bluebird');

var async = require('asyncawait/async');
var await = require('asyncawait/await');


module.exports = {


    schema:'true',

    attributes: {

        give : 'json',

        recieve : 'json',

        isClosed : {
            type : 'boolean',
            defaultsTo : false
        },

        support : {
            type : 'json',
            defaultsTo: false
        },

        messages : {
            collection : 'messages',
            via : "conversation"
        }
    },

    afterCreate : (values, cb) => {

        sails.sockets.broadcast('notification', 'conversation_notification', values);

        sails.sockets.broadcast('supports', 'room_created', values);
        cb();
    },

    afterUpdate : (values, cb) => {



        if (values.isClosed) {

            console.log('room_' + values.id);

            sails.sockets.broadcast('room_' + values.id, 'room_closed', values);
            sails.sockets.broadcast('supports', 'room_closed', values);
        } else {
            sails.sockets.broadcast('supports', 'room_updated', values);
        }

        cb();
    },

    afterDestroy : (values, cb) => {
        sails.sockets.broadcast('supports', 'room_closed', values);
        sails.sockets.broadcast(values.id + '_room', 'room_closed', values);
        cb();
    },

    changeSupport : async((currentConversation, supportInfo) => {


        if (!currentConversation.support) {

            let oldConversations = await(
                Conversation.find({
                    support : {
                        support_id : supportInfo.support_id
                    }
                })
            );

            if (oldConversations.length) {
                oldConversations.forEach(function (nextConv) {

                    if (nextConv.id === currentConversation.id) {
                        return ;
                    }

                    nextConv.support = false;
                    await(
                        nextConv.save()
                    );

                    sails.sockets.broadcast('supports', 'support_changed', nextConv);
                });
            }

            currentConversation.support = supportInfo;
            await(currentConversation.save());


            sails.sockets.broadcast('supports', 'support_changed', currentConversation);
        }

        return currentConversation;
    })

};
