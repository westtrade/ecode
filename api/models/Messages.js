/**
* Messages.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
    schema: true,
    attributes: {
        text : {
            type : "string",
            required: true
        },

        from : {
            type: "string",
            required: true
        },
        date : {
            type: "string",

        },
        conversation : {
            model : "conversation",
            required: true
        }
    },


    afterCreate : function (values, cb) {

        sails.sockets.broadcast('conversation_' + values.conversation, 'message', values);
        sails.sockets.broadcast('notification', 'message_notification', values);

        cb();
    }
};
