/**
* Codes.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var uuid = require('node-uuid');


module.exports = {

    autoPK: false,

    attributes: {

        code : {
            type: 'string',
            primaryKey: true,
            unique: true,
            index: true,
            uuidv4: true
        },

        amount : {
            type : 'float',
            required : true,
            min: 0,
            defaultsTo: 0
        },

        blocked : {
            type : 'boolean',
            defaultsTo : false
        }
    },

    beforeValidate: function(values, next) {
        values.code = uuid.v4();
        next();
    }


};
