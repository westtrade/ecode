/**
 * NewsController
 *
 * @description :: Server-side logic for managing news
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


    full_page : function (req, res, next) {

        //News.find()
        //res.send('ok');

        //res.send(req.params.news_id);


        var news_id = req.param('news_id', -1);
        news_id = parseInt(news_id);

        if (isNaN(news_id)) news_id = -1;

        News
            .find({ index : news_id})
            .limit(1)
            .then(function (news_list) {

                if (!news_list.length) {
                    return res.notFound();
                }

                res.view('news/fullpage', {
                    current_news : news_list[0],
                    support_online : false
                });
            })

            .catch(function (e) {
                res.serverError(e);
            })



    },

    news_list_page : function (req, res, next) {

        var newsPerPage = 5;
        var currentPage = req.param('page', 1);
        currentPage = parseInt(currentPage);

        News
            .find()
            .sort({ 'index' : 'desc' })
            .paginate({page: currentPage, limit: newsPerPage })
            .then(function (news) {

                return News.count().then(function (count) {

                    var totalPages = parseInt(count / newsPerPage) + (count % newsPerPage === 0 ? 0 : 1);
                    var pagerCountBefore = currentPage < 5 ? currentPage : 5;
                    var pagerCountAfter = totalPages - currentPage < 5 ? (totalPages - currentPage) + 1 : 5 ;

                    res.view('news/list', {
                        'news' : news,
                        'currentPage' : currentPage,
                        'total_news' : count,
                        'total_pages' : totalPages,
                        'news_limit' : newsPerPage,
                        'pager_count_before' : pagerCountBefore,
                        'pager_count_after' : pagerCountAfter,
                        support_online : false
                    });
                });

            }).catch(function (e) {
                return res.serverError(e);
            })

            ;

    }



};

