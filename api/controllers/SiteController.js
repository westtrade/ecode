/**
 * SiteController
 *
 * @description :: Server-side logic for managing Sites
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

"use strict";

var uuid = require('node-uuid');

var async = require('asyncawait/async');
var await = require('asyncawait/await');

module.exports = {

    "chat_page" : async((req, res) => {

        var give = req.param('give');
        var recieve = req.param('recieve');

        var give_currencie = give in websiteSettings.currencies ? websiteSettings.currencies[give] : null;
        var recieve_currencie = recieve in websiteSettings.currencies ? websiteSettings.currencies[recieve] : null;

        if (give_currencie) give_currencie.code = give;
        if (recieve_currencie) recieve_currencie.code = recieve;


        let conversationIsCorrect =
            !give_currencie ||
            !recieve_currencie ||
            ((give === 'cc') && (recieve === 'cc'))
        ;

        let currentConversation = conversationIsCorrect ? null : await(
                Conversation.create({
                    give : give_currencie,
                    recieve : recieve_currencie
                })
            );


        res.view("chat/room", {
            conversation : await(Conversation.findOne(currentConversation.id).populate('messages'))
        });
    }),

    "start_conversation" : async((req, res) => {

        let currentConversation = await(
            Conversation.findOne(req.param('conversation_id', null)).populate('messages')
        );

        if (!currentConversation) {
            return res.serverError("Cщnversation dosn't exists!");
        }

        //console.log(currentConversation);

        sails.sockets.join(req.socket, 'users');
        sails.sockets.join(req.socket, 'conversation_' + currentConversation.id);

        if (!req.session.conversation_ids) {
            req.session.conversation_ids = [];
        }

        if (req.session.conversation_ids.indexOf(currentConversation.conversation_id) < 0) {
            req.session.conversation_ids.push(currentConversation.conversation_id);
        }

        //console.log(req.session.conversation_ids);
        //console.log(Object.keys(websiteSettings.supports), websiteSettings.supports);

        res.json({
            support_online : Object.keys(websiteSettings.supports).length > 0,
            conversation: currentConversation
        });
    }),


    "send_message" : async((req, res) => {

        /*
        var from = req.param('from');
        var data = req.param('data');
        var conversation_id = req.param('conver');
        var date = req.param('date');
        */

        //TODO Save message to log

        //console.log(req.params.all());

        let message = req.params.all();
        message.from = "user";

        message = await(
            Messages.create(message)
        );

        res.json(message);
    }),


    "home_page" : async((req, res, next) => {

        let newsList = await(
            News
                .find()
                .sort({ 'createdAt' : 'desc' })
                .limit(4)
        );

        var variables = {
            support_online : Object.keys(websiteSettings.supports).length > 0,
            news : newsList
        };

        res.view("homepage", variables);
    }),




    "code_info" : async((req, res) => {

        let currentCode = await(Codes.findOne( req.param('code', null) ));

        let result = {
            code : currentCode.code
        };

        if (!currentCode) {

            result.error = {
                code : 404,
                message : 'Code dosn`t exist'
            };

        } else if (currentCode.blocked) {

            result.error = {
                code : 403,
                message : 'Code is blocked'
            };

        } else {

            result.amount = parseFloat(currentCode.amount);
        }

        res.json(result);
    })
};
