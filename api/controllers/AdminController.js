/**
 * AdminController
 *
 * @description :: Server-side logic for managing admins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

"use strict";

var Q = require('q');
var uuid = require('node-uuid');

var async = require('asyncawait/async');
var await = require('asyncawait/await');


module.exports = {

    index : function (req, res) {

        var itemsPerPage = 10;
        var currentPage = req.param('page', 1);
        currentPage = parseInt(currentPage);

        var editCodeIdx = req.param('edit', false);

        Q.all([
            Codes.findOne({ code : editCodeIdx }),
            Codes.find().sort({ 'createdAt' : 'desc' }).paginate({page: currentPage, limit: itemsPerPage }),
            Codes.count()
        ]).spread(function (editCodeIdx, code_list, count) {

            var totalPages = parseInt(count / itemsPerPage) + (count % itemsPerPage === 0 ? 0 : 1);
            var pagerCountBefore = currentPage <= 5 ? currentPage : 5;
            var pagerCountAfter = totalPages - currentPage < 5 ? (totalPages - currentPage) + 1 : 5 ;

            req.session.support_id = 'support_id' in req.session && req.session.support_id ? req.session.support_id : uuid.v4();
            var current_support_id = req.session.support_id ;

            if (!(current_support_id in websiteSettings.supports)) {
                websiteSettings.supports[current_support_id] = {
                    login : req.session.login
                }
            }

            res.view('admin/index', {
                'layout' : '/admin/layout' ,
                'codes' : code_list,
                'currentPage' : currentPage,
                'items_count' : count,
                'total_pages' : totalPages,
                'items_limit' : itemsPerPage,
                'pager_count_before' : pagerCountBefore,
                'pager_count_after' : pagerCountAfter,
                'edit_code' : editCodeIdx ? editCodeIdx.toObject() : null,
                "support_id" : current_support_id,
                "rooms" : websiteSettings.chatRooms,
                "supports" : websiteSettings.supports
            });
        })
        .catch(function (error) {
            return res.serverError(error);
        });
    },

    send_message : async((req, res) => {


        /*var from = req.param('from');
        var data = req.param('data');
        var conversation_id = req.param('conversation_id');
        var date = req.param('date');*/

        //TODO Save message to log


        let message = req.params.all();


        console.log(message);

        message.from = 'support';

        message = await(
            Messages.create(message)
        );

        res.json(message);

        /*

        Messages.create({
            name : 'support',
            text : data,
            date : date,
            conversation_id : conversation_id
        }).then(function (savedMessage) {

            sails.sockets.broadcast('user_' + conversation_id, 'message', {
                from : from,
                data : data,
                conversation_id : conversation_id,
                date : data
            });



        });
        */
    }),


    get_chat_log : function (req, res) {


        var conversation_id = req.param('conversation_id');

        Messages
            .find({ conversation_id :  conversation_id })
            .then(function (messages) {
                res.json(messages);
            })
            .catch(function (e) {
                res.serverError(e);
            });
    },

    signin : function (req, res, next) {

        if (req.session.authenticated) {
            return res.redirect('/admin');
        }

        var login = req.param('login');
        var password = req.param('password');

        req.session.login = login;
        req.session.password = password;


        var isTest = req.param('test', false);

        if (sails.config.environment === 'development' && isTest) {
            var login = req.param('login', 'dealer');
            var password = req.param('password', 'wPsquQxR');
        }

        if (login in userList.users && userList.users[login] === password) {
            req.session.authenticated = true;
            return res.redirect('/admin');
        }

        req.flash('error', 'User not found');
        return res.redirect('/admin/signin');

    },


    conversation_rooms : async((req, res) => {


        req.session.support_id = 'support_id' in req.session && req.session.support_id ? req.session.support_id : uuid.v4();

        var current_support_id = req.session.support_id ;

        if (!(current_support_id in websiteSettings.supports)) {
            websiteSettings.supports[current_support_id] = {
                login : req.session.login
            }
        }


        sails.sockets.join(req.socket, 'supports');
        sails.sockets.join(req.socket, 'sup_' + current_support_id);
        sails.sockets.join(req.socket, 'rooms');

        sails.sockets.broadcast('users', 'support_online', Object.keys(websiteSettings.supports).length > 0 );

        let totalConversations = await(
            Conversation
                .find({ isClosed : false })
                .sort({ 'createdAt' : 'asc' })
        );


        var result = {
            support_id : current_support_id,
            conversations : totalConversations,
            supports : websiteSettings.supports
        };

        res.json(result);
    }),


    close_room : async((req, res) => {

        let conv = await(
            Conversation.findOne(req.param('conversation_id', null))
        ) || null;


        if (!conv) {
            return res.serverError("Conversation not found!");
        }

        conv.isClosed = true;
        await(conv.save());

        return res.json(conv);

    }),


    admin_notification : async((req, res) => {

        if (req.isSocket) {
            sails.sockets.join(req.socket, 'notification');
        }

        res.json({
            status: 'ok'
        });

    }),


    chat_page : async((req, res) => {

        let current_support_id = req.session.support_id ;

        let currentConversation = await(

            Conversation.findOne(
                req.param('conversation_id', -1)
            ).populate('messages')

        ) || null;


        let fullSupportInfo = websiteSettings.supports[current_support_id];
        fullSupportInfo.support_id = current_support_id;

        if ('password' in fullSupportInfo) {
            delete fullUserInfo.password;
        }


        if (currentConversation && req.isSocket) {
            sails.sockets.join(req.socket, 'conversation_' + currentConversation.id);

            console.log("Admin subscrive to messages from: %s", 'conversation_' + currentConversation.id);

            currentConversation = await(
                Conversation.changeSupport(currentConversation, fullSupportInfo)
            );

        }


        let totalConversations = await(
            Conversation
                .find({ isClosed : false })
                .sort({ 'createdAt' : 'asc' })
        );


        let data = {
            conversation: currentConversation,
            conversations: totalConversations,
            support_id : current_support_id,
            supports : websiteSettings.supports,
            layout : '/admin/layout'
        };


        if (req.wantsJSON) {
            res.json(data);
        } else {
            res.view('admin/chat', data);
        }
    }),


    news_page : function (req, res, next) {

        var newsPerPage = 10;
        var currentPage = req.param('page', 1);
        currentPage = parseInt(currentPage);


        var editNewsIdx = parseInt(req.param('edit', false));


        News
            .findOne({ index : editNewsIdx })
            .then(function (editNews) {

                News
                    .find()
                    .sort({ 'index' : 'desc' })
                    .paginate({page: currentPage, limit: newsPerPage })
                    .then(function (news) {

                        return News.count().then(function (count) {

                            var totalPages = parseInt(count / newsPerPage) + (count % newsPerPage === 0 ? 0 : 1);
                            var pagerCountBefore = currentPage <= 5 ? currentPage : 5;
                            var pagerCountAfter = totalPages - currentPage < 5 ? (totalPages - currentPage) + 1 : 5 ;

                            res.view('admin/news', {
                                'layout' : '/admin/layout' ,
                                'news' : news,
                                'currentPage' : currentPage,
                                'total_news' : count,
                                'total_pages' : totalPages,
                                'news_limit' : newsPerPage,
                                'pager_count_before' : pagerCountBefore,
                                'pager_count_after' : pagerCountAfter,
                                'edit_news' : editNews ? editNews.toObject() : null,
                                "rooms" : websiteSettings.chatRooms,
                                "supports" : websiteSettings.supports
                            });

                        });

                    }).catch(function (e) {
                        return res.serverError(e);
                    })

                    ;




            })
            .catch(function (error) {
                return res.serverError(error);
            })
            ;
    },


    create_news : function (req, res) {

        var title = req.param('title', '');
        var content = req.param('content', '');

        if (!title.length || !content.length) {
            req.flash('error', 'Title and content is required fields');
            return res.redirect('admin/news') ;
        }



        News.create({
            title : title,
            content : content
        }, function (err, news) {

            if (err) {
                console.log(err)
            };


            res.redirect('admin/news')  ;

        });
    },

    edit_news : function (req, res) {

        var index = req.param('index', false);

        if (index === false || parseInt(index) === NaN) {
            req.flash('error', 'News dosn`t exists');
            return res.redirect('/admin/news?edit=' + index);
        }


        var title = req.param('title', false);
        var content = req.param('content', false);

        if ((title && !title.length) || (content && !content.length)) {
            req.flash('error', 'Fields cant be empty');
            return res.redirect('/admin/news?edit=' + index);
        }

        News
            .findOne({ index : parseInt(index)})
            .then(function (edit_news) {
                if (!edit_news) {
                    req.flash('error', 'News dosn`t exists');
                    return res.redirect('/admin/news?edit=' + index);
                }

                if(title) edit_news.title = title;
                if(content) edit_news.content = content;

                edit_news.save(function () {
                    return res.redirect('/admin/news');
                });
            })
            .catch(function (err) {
                res.serverError(err);
            })
        ;
    },

    delete_news : function (req, res, next) {

        var index = req.param('index', false);

        if (index === false || parseInt(index) === NaN) {
            req.flash('error', 'News dosn`t exists');
            return res.redirect('/admin/news?edit=' + index);
        }


        var id = req.param('id', null);

        var query = {
            index : parseInt(index)
        };

        if (id) {
            query['id'] = id;
        }

        News
            .findOne(query)
            .then(function (editNews) {

                if (id && editNews) {

                    if (editNews) {
                        editNews.destroy();
                        req.flash('message', 'News deleted successfull');
                        return res.redirect('/admin/news');
                    } else {
                        req.flash('message_error', 'News ' + index +  ' dosn`t exists');
                        return res.redirect('/admin/news');
                    }
                }

                return res.view(
                    'admin/confirm_delete_news', {
                        'layout' : '/admin/layout' ,
                        deleted_news : editNews,
                        "rooms" : websiteSettings.chatRooms,
                        "supports" : websiteSettings.supports
                    }
                );
            })

            .catch(function (err) {
                res.serverError(err);
            })
        ;
    },


    signout : function  (req, res) {


        if ('support_id' in req.session && req.session.support_id && req.session.support_id in websiteSettings.supports) {
            delete websiteSettings.supports[req.session.support_id];
        }

        req.session.authenticated = false;

        req.session.destroy(function(err) {});

        sails.sockets.broadcast('users', 'support_online', Object.keys(websiteSettings.supports).length > 0 );
        res.redirect('/admin/signin');
    },



    create_cash_code : async((req, res) => {

        var amount = req.param('amount', null);

        if (!amount) {
            req.flash('error', 'Amount is required fields');
            return res.redirect('admin') ;
        }

        amount = parseFloat(amount);

        let code = await(Codes.create({amount : amount}));

        if (req.wantsJSON) {
            return res.json(code);
        }

        res.redirect('admin');
    }),

    edit_cash_code : function (req, res) {

        var code = req.param('code', false);

        if (code === false ) {
            req.flash('error', 'Code dosn`t exists');
            return res.redirect('/admin?edit=' + code);
        }


        var amount = req.param('amount', false);

        if ((amount && !amount.length)) {
            req.flash('error', 'Amount can`t be empty');
            return res.redirect('/admin?edit=' + code);
        }

        Codes
            .findOne({ code : code })
            .then(function (edit_code) {
                if (!edit_code) {
                    req.flash('error', 'Code dosn`t exists');
                    return res.redirect('/admin/news?edit=' + code);
                }

                edit_code.amount = parseFloat(amount);

                edit_code.save(function () {
                    return res.redirect('/admin');
                });
            })
            .catch(function (err) {
                res.serverError(err);
            })
        ;
    },

    delete_cash_code : function (req, res, next) {

        var code = req.param('code', false);

        if (code === false) {
            req.flash('error', 'Code dosn`t exists');
            return res.redirect('/admin?edit=' + index);
        }

        var id = req.param('id', false);

        Codes
            .findOne({ code : code })
            .then(function (editCode) {

                if (!editCode) {
                    req.flash('message_error', 'Code ' + code +  ' dosn`t exists');
                    return res.redirect('/admin');
                }

                if (id && editCode) {
                    editCode.destroy();
                    req.flash('message', 'Code deleted successfull');
                    return res.redirect('/admin');
                }

                return res.view(
                    'admin/confirm_delete_code', {
                        'layout' : '/admin/layout' ,
                        deleted_code : editCode,
                        type : 'delete',
                        "rooms" : websiteSettings.chatRooms,
                        "supports" : websiteSettings.supports
                    }
                );
            })

            .catch(function (err) {
                res.serverError(err);
            })
        ;
    },

    block_cash_code : function (req, res, next) {

        var code = req.param('code', false);

        if (code === false) {
            req.flash('error', 'Code dosn`t exists');
            return res.redirect('/admin?edit=' + index);
        }

        var id = req.param('id', false);

        Codes
            .findOne({ code : code })
            .then(function (editCode) {

                if (!editCode) {
                    req.flash('message_error', 'Code ' + code +  ' dosn`t exists');
                    return res.redirect('/admin');
                }

                if (id && editCode) {
                    editCode.blocked = !editCode.blocked;
                    editCode.save();

                    req.flash('message', 'Code deleted successfull');
                    return res.redirect('/admin');
                }

                return res.view(
                    'admin/confirm_delete_code', {
                        'layout' : '/admin/layout' ,
                        deleted_code : editCode,
                        type : 'block',
                        "rooms" : websiteSettings.chatRooms,
                        "supports" : websiteSettings.supports
                    }
                );
            })

            .catch(function (err) {
                res.serverError(err);
            })
        ;
    },









};
