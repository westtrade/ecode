/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

    /***************************************************************************
    *                                                                          *
    * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
    * etc. depending on your default view engine) your home page.              *
    *                                                                          *
    * (Alternatively, remove this and add an `index.html` file in your         *
    * `assets` directory)                                                      *
    *                                                                          *
    ***************************************************************************/

    '/': 'SiteController.home_page',
    '/news': 'NewsController.news_list_page',
    '/request/:give/:recieve': 'SiteController.chat_page',
    '/news/:news_id' : 'NewsController.full_page',
    '/start_conversation/:conversation_id' : 'SiteController.start_conversation',
    '/send_message' : 'SiteController.send_message',


    'GET /admin/signin' : {
        'view' : 'admin/login',
        'locals' : {
            'layout' : 'admin/layout_login'
        }
    },

    'POST /admin/signin' : 'AdminController.signin',
    '/admin' : 'AdminController.index',
    '/admin/codes' : 'AdminController.index',
    '/admin/news' : 'AdminController.news_page',
    '/admin/news/create' : 'AdminController.create_news',
    '/admin/news/edit' : 'AdminController.edit_news',
    '/admin/news/delete/:index' : 'AdminController.delete_news',

    '/admin/chat/:conversation_id/close' : 'AdminController.close_room',

    '/admin/chat/:conversation_id' : 'AdminController.chat_page',
    '/admin/chat' : 'AdminController.chat_page',
    '/admin/notification' : 'AdminController.admin_notification',






    '/admin/send_message' : 'AdminController.send_message',
    '/admin/select_room/:conversation_id' : 'AdminController.get_chat_log',


    '/admin/create' : 'AdminController.create_cash_code',
    '/admin/signout' : 'AdminController.signout',
    '/admin/delete/:code' : 'AdminController.delete_cash_code',
    '/admin/edit/:code'   : 'AdminController.edit_cash_code',
    '/admin/block/:code'   : 'AdminController.block_cash_code',
    '/admin/conversation/rooms' : 'AdminController.conversation_rooms',
    '/api/codes/:code' : 'SiteController.code_info'





    /***************************************************************************
    *                                                                          *
    * Custom routes here...                                                    *
    *                                                                          *
    * If a request to a URL doesn't match any of the custom routes above, it   *
    * is matched against Sails route blueprints. See `config/blueprints.js`    *
    * for configuration options and examples.                                  *
    *                                                                          *
    ***************************************************************************/

};
