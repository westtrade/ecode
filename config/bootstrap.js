/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

var _ = require('lodash');

module.exports.bootstrap = function(cb) {


    _.extend(sails.hooks.http.app.locals, sails.config.http.locals);

    sails.on('lifted', function () {
        Sessions.find({ 'session' : { '$exists' : true } }).then(function (sessions) {

            sessions.forEach(function (sessionData) {
                sessionData = JSON.parse(sessionData.session);

                if ('support_id' in sessionData && sessionData.support_id) {
                    websiteSettings.supports[sessionData.support_id] = {
                        login : sessionData.login
                    }

                    //console.log(websiteSettings.supports[sessionData.support_id]);
                }


            });

        });
    });


    // It's very important to trigger this callback method when you are finished
    // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
    cb();
};
